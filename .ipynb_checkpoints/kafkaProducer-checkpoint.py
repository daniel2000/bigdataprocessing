import json
from kafka import KafkaProducer
import netCDF4
import time as t
import numpy.ma as ma
import os


# Creating the producer
# producer = KafkaProducer(bootstrap_servers='localhost:9092',
#                          value_serializer=lambda x: json.dumps(x).encode('utf-8'),
#                          )
print("Connected to producer")

# Obtaining the actual variable name
def find_variable_name(variables):
    known = ['time', 'time_bnds', 'lat', 'lat_bnds', 'lon', 'lon_bnds','height']
    for var in variables:
        if var not in known:
            return var



directory = "dataset"


for filename in os.listdir(directory):

    if 'pr' not in filename:
        continue

    file = os.path.join(directory, filename)
    nc = netCDF4.Dataset(file, mode='r')

    variable = find_variable_name(nc.variables.keys())

    for time in range(len(nc.variables['time'])):
        for lat in range(len(nc.variables['lat'])):
            for lon in range(len(nc.variables['lon'])):

                # Generating a dictionary to pass to the topic
                record = {}
                for var in nc.variables.keys():
                    if var == 'time':
                        record['timevalue'] = ma.getdata(nc.variables[var])[time]
                    elif var == 'lat':
                        record[var] = ma.getdata(nc.variables[var])[lat]
                    elif var == 'lon':
                        record[var] = ma.getdata(nc.variables[var])[lon]
                    elif var == variable:
                        record[var] = float(ma.getdata(nc.variables[var])[time][lat][lon])

                print(record)
                # Sending each record on its respective topic
                # producer.send(variable,value=record)
        t.sleep(5)
    print('waiting')
    t.sleep(5)
    print('Done wait')

# # wait until all messages go through
# producer.flush()


