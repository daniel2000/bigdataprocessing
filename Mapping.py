from pyflink.common import Types
from pyflink.datastream import RuntimeContext, ReduceFunction, FlatMapFunction
from pyflink.datastream.state import ValueStateDescriptor

class MonthAverage(FlatMapFunction):

    def __init__(self):
        self.sum = None

    def open(self, runtime_context: RuntimeContext):
        descriptor = ValueStateDescriptor(
            "average",  # the state name
            Types.PICKLED_BYTE_ARRAY()  # type information
        )
        self.sum = runtime_context.get_state(descriptor)

    def flat_map(self, value):
        current_sum = self.sum.value()
        if current_sum is None:
            current_sum = (0, 0)

        # Updating the count and the total
        current_sum = (current_sum[0] + 1, current_sum[1] + value[3])

        self.sum.update(current_sum)

        # Once a count of 7680 (60*128) has been reached, this means that a months worth of data have been processed
        # and therefore the month average can be calculated
        if current_sum[0] == 7680:
            self.sum.clear()
            yield value[0], current_sum[1] / current_sum[0]