# Big Data Processing

Daniel Attard, Andrew Joseph Magri

## Data

Due to size limitations, the original dataset, stream process results, and data storage, were not attached.

The full repository can be found at https://gitlab.com/daniel2000/bigdataprocessing

## Docker
To run the system, start by going to the main directory and running the following commands

To generate the custom image
		
`sudo docker build --tag stream:latest -f stream.Dockerfile .`

To start up zookeeper, kafka server, topic generator, and producer and consumer and notebook-runner
		
`sudo docker-compose up`

In the console output where docker-compose is run, the kafka-flink instance will output a link to a jupyter notebook (hosted on port 80).
Such link can be used from your local machine to access the jupyter notebook on the instance from which one can run 
the 'kafkaProducer.ipynb' and 'dataStreaming.ipynb'.

Another jupyter notebook link is outputted by the notebook-runner (hosted on port 8888). This can be used to run the 'forcasting.ipynb' and 'model.ipynb' for the models and 'visualisations.ipynb' for the visualisations 

All the required installations, including python modules, are preinstalled through the custom docker image.




## Files

kafkaProducer.ipynb is used to produce the data.

dataStreaming.ipynb is used to consume the data while also stream processing it to obtain the average and storing the data into tables.

Visualisation.ipynb can be used to generate the visualisation both for the average and whole dataset.

model.ipynb can be used to run the prediction models.

forecasting.ipynb can be used to run the forcasting models.

Mapping.py stores the flatmap function for stream processing.

master_df.csv contains the whole stored data.

master_averages_df.csv contains the global average of each variable per month.

generate_average_CSV_files.py and merge_files.py are used to combine the extracted data.
