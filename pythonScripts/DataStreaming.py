from pyflink.common import WatermarkStrategy, JsonRowDeserializationSchema, Types, Encoder, Row
from pyflink.datastream import TimeCharacteristic, StreamExecutionEnvironment, CountWindow, WindowAssigner
from pyflink.datastream.connectors import FlinkKafkaConsumer, FileSink, OutputFileConfig, StreamingFileSink
from pyflink.table import StreamTableEnvironment
from pyflink.table.window import GroupWindow, Tumble

from Mapping import MonthAverage

from functools import reduce
import pandas as pd

def get_json_row_schema_by_var(var):
    rows = ['timevalue', 'lat', 'lon', var]

    type_info = Types.ROW_NAMED(rows,
                                [Types.DOUBLE(), Types.DOUBLE(), Types.DOUBLE(), Types.DOUBLE()])
    json_row_schema = JsonRowDeserializationSchema.builder().type_info(type_info).build()

    return json_row_schema


def stream_process_data():
    env = StreamExecutionEnvironment.get_execution_environment()
    env.add_jars("file:///home/danny/Downloads/flink-sql-connector-kafka_2.11-1.11.6.jar")
    # env.add_jars("file:///C:/Users/andre/Downloads/flink-sql-connector-kafka_2.11-1.11.6.jar")

    t_env = StreamTableEnvironment.create(stream_execution_environment=env)

    env.set_parallelism(1)
    env.set_stream_time_characteristic(TimeCharacteristic.EventTime)

    kafka_props = {'bootstrap.servers': 'localhost:9092', 'group.id': 'None'}
    variable_names = ["ps","clt","evspsbl","hfss","huss","pr","prsn","psl","rlus","rsds","rsus","tas"]

    consumers = []
    datastreams = []
    tables = []

    for i,variable_name in enumerate(variable_names):
        # Creating a consumer for the kafka message broker from each topic
        consumers.append(FlinkKafkaConsumer(variable_name, get_json_row_schema_by_var(variable_name), kafka_props))
        consumers[i].set_start_from_earliest()

        # Creating a datastream for each consumer
        datastreams.append(env.add_source(consumers[i]))

        # Creating a table
        t_env.execute_sql(f"""
                CREATE TABLE {variable_name}_sink (
                  timevalue DOUBLE ,
                  lat DOUBLE,
                  lon DOUBLE,
                  {variable_name} DOUBLE
                ) WITH (
                  'connector' = 'filesystem',
                  'format' = 'csv',
                  'path' = 'dataTables/{variable_name}Table'
                )
        """)

    # Adding each datastream values to a table
    for i in range(len(datastreams)):
        tables.append(t_env.from_data_stream(datastreams[i]))
        tables[i].execute_insert(variable_names[i] + '_sink')



    for i in range(len(datastreams)):
        datastreams[i].print()
        datastreams[i] = datastreams[i].key_by(lambda x:x[0]).flat_map(MonthAverage(), output_type=Types.TUPLE([Types.DOUBLE(), Types.DOUBLE()]))

        output_path = 'averages/' + variable_names[i]
        datastreams[i].add_sink(StreamingFileSink
                    .for_row_format(output_path, Encoder.simple_string_encoder())
                    .build())

    env.execute("Stream Process the Data")


stream_process_data()

