import matplotlib.pyplot as plt
import numpy as np; np.random.seed(0)
import seaborn as sns
import pandas as pd


variables = [('tas','Near-Surface Air Temperature'),('prsn','Snowfall Flux'),('pr','Precipitation'),('evspsbl','Water Evaporation'),
 ('huss','Near-Surface Specific Humidity'),('clt','Total Cloud Fraction'),('psl','Sea Level Pressure'),('rlus','surface_upwelling_longwave_flux_in_air'),
 ('rsus','Surface Upwelling Shortwave Radiation'),('rsds','Surface Downwelling Shortwave Radiation'),('hfss','Surface Upward Sensible Heat Flux')
 ]

def visuals():
    fig, axes = plt.subplots(1, sharey=True)
    data = pd.read_csv('../masterData/master_df.csv', index_col=0)
    data.lon = data.lon.round(0)
    data.lat = data.lat.round(0)

    # timevalue = 733300.5 - year 0
    # timevalue = 733451.0 - year 0.5
    # timevalue = 735094.5 - year 5
    # Times 6 months apart
    timevalues =[733300.5,733451.0]


    for variable in variables:
        fig, axes = plt.subplots(1, 2, figsize=(15, 5), sharey=True)
        fig.suptitle(f'Heat maps 6 months apart for variable `{variable[0]}` \n `{variable[1]}`')
        for i,time in enumerate(timevalues):
            dataMod = data.loc[data['time'] == timevalues[i]]
            pivotedData = dataMod.pivot("lat", "lon", variable[0])
            sns.heatmap(pivotedData, ax=axes[i])
            # axes[i].set_title(f'Time= {timevalues[i]}')



        plt.show()

    timevalues =[733300.5,735094.5]

    for variable in variables:
        fig, axes = plt.subplots(1, 2, figsize=(15, 5), sharey=True)
        print(variable[0])
        # fig.suptitle(f'Heat maps 5 years apart for variable `{variable[0]}` \n `{variable[1]}`')
        for i, time in enumerate(timevalues):
            dataMod = data.loc[data['time'] == timevalues[i]]
            pivotedData = dataMod.pivot("lat", "lon", variable[0])
            sns.heatmap(pivotedData, ax=axes[i])
            axes[i].set_title(f'{i+1}')

        plt.show()

def average_visuals():
    data = pd.read_csv('../masterData/master_averages_df.csv', index_col=0)
    for i,variable in enumerate(variables):
        print(variable[0])
        fig, axes = plt.subplots(1, 1, figsize=(10, 5), sharey=True, sharex=False)
        sns.set_theme(style="whitegrid")
        temp = data[variable[0]+'_avg']
        # fig.suptitle(f'Line plot of average value per month for variable `{variable[0]}` \n `{variable[1]}`')
        sns.lineplot(data=temp, palette="tab10", linewidth=2.5)

        plt.show()


average_visuals()
visuals()
