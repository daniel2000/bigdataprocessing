import os
from functools import reduce

import pandas as pd

# for subdir, dirs, files in os.walk('averages'):
#     # for file in files:
#     if not os.path.exists(subdir + 'Mod'):
#         os.mkdir(subdir + 'Mod')
#
#     for f in files:
#         fw = open(os.path.join(subdir+'Mod', f),'w',encoding='utf-8')
#         f = open(os.path.join(subdir, f),'r+',encoding='utf-8')
#         lines = f.readlines()
#         newlines = []
#         for l in lines:
#             l = l.replace('(','')
#             l = l.replace(')', '')
#             newlines.append(l)
#
#
#         fw.writelines(newlines)
#         f.close()
#


import os

directory = os.fsencode('averages')

done = [0,0,0,0,0,0,0,0,0,0,0,0]

import os
clt_df = pd.DataFrame()
evspsbl_df = pd.DataFrame()
hfss_df = pd.DataFrame()
huss_df = pd.DataFrame()
prsn_df = pd.DataFrame()
pr_df = pd.DataFrame()
psl_df = pd.DataFrame()
ps_df = pd.DataFrame()
rlus_df = pd.DataFrame()
rsds_df = pd.DataFrame()
rsus_df = pd.DataFrame()
tas_df = pd.DataFrame()

for subdir, dirs, files in os.walk('../averages'):
    # for file in files:
    if 'Mod' in subdir:
        if 'clt' in subdir:
            dfs = (pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'clt_avg']) for f in files)
            temp = pd.concat(dfs,ignore_index=True)
            clt_df = pd.concat([clt_df,temp],ignore_index=True)
        elif 'evspsbl' in subdir:
            dfs = (pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'evspsbl_avg']) for f in files)
            temp = pd.concat(dfs, ignore_index=True)
            evspsbl_df = pd.concat([evspsbl_df,temp],ignore_index=True)
        elif 'hfss' in subdir:
            dfs = (pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'hfss_avg']) for f in files)
            temp = pd.concat(dfs, ignore_index=True)
            hfss_df = pd.concat([hfss_df,temp],ignore_index=True)
        elif 'huss' in subdir:
            dfs = (pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'huss_avg']) for f in files)
            temp = pd.concat(dfs, ignore_index=True)
            huss_df = pd.concat([huss_df, temp], ignore_index=True)
        elif 'prsn' in subdir:
            dfs = (pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'prsn_avg']) for f in files)
            temp = pd.concat(dfs, ignore_index=True)
            prsn_df = pd.concat([prsn_df, temp], ignore_index=True)
        elif 'pr' in subdir:
            dfs = (pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'pr_avg']) for f in files)
            temp = pd.concat(dfs,ignore_index=True)
            pr_df = pd.concat([pr_df, temp], ignore_index=True)
        elif 'psl' in subdir:
            dfs = (pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'psl_avg']) for f in files)
            temp = pd.concat(dfs, ignore_index=True)
            psl_df = pd.concat([psl_df, temp], ignore_index=True)
        elif 'ps' in subdir:
            dfs = (pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'ps_avg']) for f in files)
            temp = pd.concat(dfs,ignore_index=True)
            ps_df = pd.concat([ps_df, temp], ignore_index=True)
        elif 'rlus' in subdir:
            dfs= (pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'rlus_avg']) for f in
                 files)
            temp = pd.concat(dfs, ignore_index=True)
            rlus_df = pd.concat([rlus_df, temp], ignore_index=True)
        elif 'rsds' in subdir:
            dfs = (pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'rsds_avg']) for f in
                 files)
            temp = pd.concat(dfs, ignore_index=True)
            rsds_df = pd.concat([rsds_df, temp], ignore_index=True)
        elif 'rsus' in subdir:
            dfs = (pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'rsus_avg']) for f in
                 files)
            temp = pd.concat(dfs, ignore_index=True)
            rsus_df = pd.concat([rsus_df, temp], ignore_index=True)

        elif 'tas' in subdir:
            dfs = (pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'tas_avg']) for f in files)
            temp = pd.concat(dfs,ignore_index=True)
            tas_df = pd.concat([tas_df, temp], ignore_index=True)
df_list = [clt_df, evspsbl_df, hfss_df, huss_df, pr_df, prsn_df, ps_df, psl_df, rlus_df, rsds_df, rsus_df, tas_df]

master_df = reduce(lambda left, right: pd.merge(left, right, on=['time'], how='outer'), df_list).fillna(0)
master_df = master_df.sort_values('time')
master_df = master_df.reset_index(drop=True)
print(master_df)

master_df.to_csv('master_averages_df.csv', sep=',')

