import pandas as pd
from functools import reduce
import os


directory = "csvs"

for subdir, dirs, files in os.walk(directory):
    # for file in files:
    if 'clt' in subdir:
        clt_df = pd.concat((pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'lat', 'lon', 'clt']) for f in files), ignore_index=True)
    elif 'evspsbl' in subdir:
        evspsbl_df = pd.concat((pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'lat', 'lon', 'evspsbl']) for f in files), ignore_index=True)
    elif 'hfss' in subdir:
        hfss_df = pd.concat((pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'lat', 'lon', 'hfss']) for f in files), ignore_index=True)
    elif 'huss' in subdir:
        huss_df = pd.concat((pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'lat', 'lon', 'huss']) for f in files), ignore_index=True)
    elif 'prsn' in subdir:
        prsn_df = pd.concat((pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'lat', 'lon', 'prsn']) for f in files), ignore_index=True)
    elif 'pr' in subdir:
        pr_df = pd.concat((pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'lat', 'lon', 'pr']) for f in files), ignore_index=True)
    elif 'psl' in subdir:
        psl_df = pd.concat((pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'lat', 'lon', 'psl']) for f in files), ignore_index=True)
    elif 'ps' in subdir:
        ps_df = pd.concat((pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'lat', 'lon', 'ps']) for f in files), ignore_index=True)
    elif 'rlus' in subdir:
        rlus_df = pd.concat((pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'lat', 'lon', 'rlus']) for f in files), ignore_index=True)
    elif 'rsds' in subdir:
        rsds_df = pd.concat((pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'lat', 'lon', 'rsds']) for f in files), ignore_index=True)
    elif 'rsus' in subdir:
        rsus_df = pd.concat((pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'lat', 'lon', 'rsus']) for f in files), ignore_index=True)
    elif 'tas' in subdir:
        tas_df = pd.concat((pd.read_csv(os.path.join(subdir, f), header=None, names=['time', 'lat', 'lon', 'tas']) for f in files), ignore_index=True)


# clt_df = pd.read_csv(
#     'cltTable/.part-37241691-cd80-438e-b2cb-c7fb7acc8da3-0-0.inprogress.03bb0c49-ba22-446e-aa44-1c48aca39de4',
#     header=None,
#     names=['time', 'lat', 'lon', 'clt']
# )
#
# evspsbl_df = pd.read_csv(
#     'evspsblTable/.part-990a2815-f696-4224-92bf-3dd90c47fbf0-0-0.inprogress.3ecf8dbc-ac66-4b0f-845e-d5e9b674096a',
#     header=None,
#     names=['time', 'lat', 'lon', 'evspsbl']
# )
#
# hfss_df = pd.read_csv(
#     'hfssTable/.part-0674f6a2-3a77-4041-b9e0-1a9eea5f508f-0-0.inprogress.0076f209-34ad-48d6-9b80-b50ff70b3d0f',
#     header=None,
#     names=['time', 'lat', 'lon', 'hfss']
# )
#
# huss_df = pd.read_csv(
#     'hussTable/.part-115be6fe-59c2-43a7-a28e-56d84328b632-0-0.inprogress.b07205cd-0ec4-4f10-a095-fae443cd1d15',
#     header=None,
#     names=['time', 'lat', 'lon', 'huss']
# )
#
# prsn_df = pd.read_csv(
#     'prsnTable/.part-7f0a067d-6d17-4166-9ce6-f57dfbc8436b-0-0.inprogress.9537cd4a-283e-4dc4-bd80-25f4f8a4d0ae',
#     header=None,
#     names=['time', 'lat', 'lon', 'prsn']
# )
#
# pr_df = pd.read_csv(
#     'prTable/.part-96968375-399c-4110-816c-c6bc208aa35e-0-0.inprogress.10d0c52a-4334-4063-81bd-00f59100570b',
#     header=None,
#     names=['time', 'lat', 'lon', 'pr']
# )
#
# psl_df = pd.read_csv(
#     'pslTable/.part-0cb77e29-8546-4438-a7f2-bf85e7969a80-0-0.inprogress.359d6587-ba94-4f3f-b886-79ca27aa74fc',
#     header=None,
#     names=['time', 'lat', 'lon', 'psl']
# )
#
# ps_df = pd.read_csv(
#     'psTable/.part-5a9e4e8a-3f79-4d6d-93af-dfb96bcba364-0-0.inprogress.e35f63d3-fec0-405e-a3b3-5326c11725e8',
#     header=None,
#     names=['time', 'lat', 'lon', 'ps']
# )
#
# rlus_df = pd.read_csv(
#     'rlusTable/.part-8da9c33d-1201-48ab-8b41-013c329e757a-0-0.inprogress.806c8698-c8bc-46ac-a362-7d70c70dabda',
#     header=None,
#     names=['time', 'lat', 'lon', 'rlus']
# )
#
# rsds_df = pd.read_csv(
#     'rsdsTable/.part-f5ef2c12-4d19-4fcf-a70d-11cbedfdc3b8-0-0.inprogress.d23cc165-acb1-4a20-92dd-76dc98e63f33',
#     header=None,
#     names=['time', 'lat', 'lon', 'rsds']
# )
#
# rsus_df = pd.read_csv(
#     'rsusTable/.part-47e4694f-1350-4140-998b-5d62a05e812d-0-0.inprogress.36e0a9de-1a0c-4cfe-b641-457d7d48957a',
#     header=None,
#     names=['time', 'lat', 'lon', 'rsus']
# )
#
# tas_df = pd.read_csv(
#     'tasTable/.part-3a2acd4e-854a-4b3d-8f46-befbc28d4c72-0-0.inprogress.c73cdd52-e449-4f3b-9788-5145fbb670b4',
#     header=None,
#     names=['time', 'lat', 'lon', 'tas']
# )
#
# print(clt_df)

df_list = [clt_df, evspsbl_df, hfss_df, huss_df, pr_df, prsn_df, ps_df, psl_df, rlus_df, rsds_df, rsus_df, tas_df]

master_df = reduce(lambda left, right: pd.merge(left, right, on=['time', 'lat', 'lon'], how='outer'), df_list).fillna(0)

print(master_df)

master_df.to_csv('master_df.csv', sep=',')

