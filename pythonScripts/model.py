import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import math

from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score, mean_absolute_error, mean_squared_error
from sklearn.experimental import enable_halving_search_cv
from sklearn.model_selection import HalvingRandomSearchCV, RandomizedSearchCV, GridSearchCV


data = pd.read_csv('../masterData/master_df.csv', index_col=0)

f = plt.figure(figsize=(19, 15))
plt.matshow(data.corr(), fignum=f.number)
plt.xticks(range(data.select_dtypes(['number']).shape[1]), data.select_dtypes(['number']).columns, fontsize=14, rotation=45)
plt.yticks(range(data.select_dtypes(['number']).shape[1]), data.select_dtypes(['number']).columns, fontsize=14)
cb = plt.colorbar()
cb.ax.tick_params(labelsize=14)
plt.title('Correlation Matrix', fontsize=16)
plt.show()

data = data.drop(columns='rlus')

train, test = np.split(data, [int(.8 * len(data))])

y_train = train['tas']
X_train = train.drop(columns='tas')

y_test = test['tas']
X_test = test.drop(columns='tas')

rf_param_grid = {'n_estimators': [100, 200, 300],
                 'max_depth': [None, 2, 6, 12],
                 'max_features': ['auto', 'sqrt', 'log2'],
                 'bootstrap': [True, False],
                 'oob_score': [True, False]}

# base_estimator = RandomForestRegressor(random_state=0, verbose=1, n_jobs=-1)
# search = HalvingRandomSearchCV(base_estimator, rf_param_grid).fit(X_train, y_train)
# returned {'oob_score': False, 'n_estimators': 100, 'max_features': 'log2', 'max_depth': None, 'bootstrap': False}
# search = RandomizedSearchCV(base_estimator, rf_param_grid).fit(X_train, y_train)
# returned {'oob_score': False, 'n_estimators': 100, 'max_features': 'log2', 'max_depth': None, 'bootstrap': False}
# search = GridSearchCV(base_estimator, rf_param_grid).fit(X_train, y_train)
# stopped early due to inefficiency

# print(search.best_params_)

# model = LinearRegression()
model = RandomForestRegressor(max_depth=None, random_state=0, verbose=1, n_jobs=-1, max_features='log2', bootstrap=False)
print('training...')
model.fit(X_train, y_train)
print('trained!')
#
y_pred = model.predict(X_test)
#
print(y_pred)
# print(model.score(X_test, y_test))
# evaluation
print('R-squared: ' + str(r2_score(y_test, y_pred)))
print('MAE: ' + str(mean_absolute_error(y_test, y_pred)))
print('RMSE: ' + str(math.sqrt(mean_squared_error(y_test, y_pred))))


